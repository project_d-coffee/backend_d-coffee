import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  Generated,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '64',
  })
  name: string;

  @Column({
    length: '10',
  })
  tel: string;

  @Column({
    length: '64',
    unique: true,
  })
  email: string;

  @Column({
    length: '256',
  })
  position: string;

  @Column({
    length: '128',
    unique: true,
  })
  password: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
