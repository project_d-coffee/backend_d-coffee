import { IsNotEmpty, Length } from 'class-validator';

export class CreateEmployeeDto {
  @IsNotEmpty()
  @Length(3, 64)
  name: string;

  @IsNotEmpty()
  position: string;

  @IsNotEmpty()
  @Length(11)
  phone: string;
}
